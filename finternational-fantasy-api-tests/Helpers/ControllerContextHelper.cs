﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace finternational_fantasy_api_tests.Helpers
{
    public static class ControllerContextHelper
    {
        public static ControllerContext GetControllerContext()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.User = new ClaimsPrincipal();

            return new ControllerContext() { HttpContext = httpContext };
        }
    }
}
