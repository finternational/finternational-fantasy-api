﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Controllers;
using Moq;
using Moq.AutoMock;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.ControllerTests
{
    public class PlayerControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResults()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var players = new List<Player>() { new Player(1, "John", "Test", 1, new Country(1, "France"), "", 2) };
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.PlayerRepository.Get(2500, 1, null, null))
                .ReturnsAsync(players);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<PlayerController>();
            var result = await sut.Get();

            Assert.Equal(players, result);
        }

        [Fact]
        public async Task GetEventsByPlayerIdReturnsCorrectResults()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var events = new List<Event>() { new Event(1, 1, 1, 1, 1) };
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.EventRepository.GetByPlayerId(1))
                .ReturnsAsync(events);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<PlayerController>();
            var result = await sut.GetEventsByPlayerId(1);

            Assert.Equal(events, result);
        }
    }
}
