﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Controllers;
using finternational_fantasy_api.Exceptions;
using finternational_fantasy_api.Security;
using finternational_fantasy_api_tests.Helpers;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.ControllerTests
{
    public class LeagueExpandedControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResultsIfLeagueIsPrivate()
        {
            var userId = Guid.NewGuid();
            var stubSettings = new PersistenceSettings("connectionstring");
            var league = new LeagueExpanded() 
            { 
                Name = "League", 
                Standings = new List<Standing>()
                { 
                    new Standing(userId, "", "", "", 1, 1) 
                } 
            };

            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.LeagueExpandedRepository.GetById(1, 1))
                .ReturnsAsync(league);

            mockUnitOfWork
                .Setup(x => x.LeagueRepository.GetById(1))
                .ReturnsAsync(new League(1, "", true, 1, Guid.NewGuid(), Guid.NewGuid(), false));

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(userId, "John", "Test"));

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<LeagueExpandedController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            var result = await sut.GetById(1, 1);

            Assert.Equal(league, result);
        }

        [Fact]
        public async Task GetReturnsCorrectResultsIfLeagueIsntPrivate()
        {
            var userId = Guid.NewGuid();
            var stubSettings = new PersistenceSettings("connectionstring");
            var league = new LeagueExpanded()
            {
                Name = "League",
                Standings = new List<Standing>()
                {
                    new Standing(Guid.NewGuid(), "", "", "", 1, 1)
                }
            };

            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.LeagueExpandedRepository.GetById(1, 1))
                .ReturnsAsync(league);

            mockUnitOfWork
                .Setup(x => x.LeagueRepository.GetById(1))
                .ReturnsAsync(new League(1, "", false, 1, Guid.NewGuid(), Guid.NewGuid(), true));

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(userId, "John", "Test"));

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<LeagueExpandedController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            var result = await sut.GetById(1, 1);

            Assert.Equal(league, result);
        }


        [Fact]
        public async Task GetThrowsForbiddenExceptionIfLeagueIsPrivateAndUserIsNotInLeague()
        {
            var userId = Guid.NewGuid();
            var stubSettings = new PersistenceSettings("connectionstring");
            var league = new LeagueExpanded()
            {
                Name = "League",
                Standings = new List<Standing>()
                {
                    new Standing(Guid.NewGuid(), "", "", "", 1, 1)
                }
            };

            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.LeagueExpandedRepository.GetById(1, 1))
                .ReturnsAsync(league);

            mockUnitOfWork
                .Setup(x => x.LeagueRepository.GetById(1))
                .ReturnsAsync(new League(1, "", true, 1, Guid.NewGuid(), Guid.NewGuid(), true));

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(userId, "John", "Test"));

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<LeagueExpandedController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            await Assert.ThrowsAsync<ForbiddenRequestException>(async () => { await sut.GetById(1, 1); });
        }
    }
}
