﻿using AutoMapper;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Controllers;
using finternational_fantasy_api.DTOs;
using finternational_fantasy_api.Exceptions;
using finternational_fantasy_api.Requests;
using finternational_fantasy_api.Security;
using finternational_fantasy_api.Verification;
using finternational_fantasy_api_tests.Helpers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.ControllerTests
{
    public class EntryControllerTests
    {
        private AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetByGameweekReturnsCorrectResult()
        {
            var teamId = Guid.NewGuid();
            var stubEntry = new Entry() { Players = new List<EntryPlayer>() };

            var stubSettings = new PersistenceSettings("connectionstring");
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.EntryRepository(teamId).GetByGameweekId(1))
                .ReturnsAsync(stubEntry);

            mockUnitOfWork
                .Setup(x => x.GameweekRepository.GetById(1))
                .ReturnsAsync(new Gameweek(1, true, new DateTime(2099, 10, 10), new DateTime(), false, false));

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            this.autoMocker.GetMock<IMapper>()
                .Setup(x => x.Map<Entry, EntryDTO>(stubEntry))
                .Returns(new EntryDTO() { Id = 1, Gameweek = 1 });

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(teamId, "John", "Test"));

            var sut = this.autoMocker.CreateInstance<EntryController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();
            var result = await sut.GetByGameweek(teamId, 1);

            Assert.Equal(1, result.Gameweek);
            Assert.Equal(1, result.Id);
        }

        [Fact]
        public async Task GetByGameweekThrowsExceptionIfDeadlineHasNotPassedAndEntryDoesNotBelongToUser()
        {
            var teamId = Guid.NewGuid();
            var stubEntry = new Entry() { Players = new List<EntryPlayer>() };

            var stubSettings = new PersistenceSettings("connectionstring");
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.EntryRepository(teamId).GetByGameweekId(1))
                .ReturnsAsync(stubEntry);

            mockUnitOfWork
                .Setup(x => x.GameweekRepository.GetById(1))
                .ReturnsAsync(new Gameweek(1, true, new DateTime(2099, 10, 10), new DateTime(), false, false));

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            this.autoMocker.GetMock<IMapper>()
                .Setup(x => x.Map<Entry, EntryDTO>(stubEntry))
                .Returns(new EntryDTO() { Id = 1, Gameweek = 1 });

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(new Guid(), "John", "Test"));

            var sut = this.autoMocker.CreateInstance<EntryController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();
             
            await Assert.ThrowsAsync<ForbiddenRequestException>(async () => await sut.GetByGameweek(teamId, 1));
        }

        [Fact]
        public async Task GetByGameweekThrowsExceptionIfTeamDoesNotHave15PlayersAndDoesNotBelongToUser()
        {
            var teamId = Guid.NewGuid();
            var stubEntry = new Entry() { Players = new List<EntryPlayer>() };

            var stubSettings = new PersistenceSettings("connectionstring");
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.EntryRepository(teamId).GetByGameweekId(1))
                .ReturnsAsync(stubEntry);

            mockUnitOfWork
                .Setup(x => x.GameweekRepository.GetById(1))
                .ReturnsAsync(new Gameweek(1, true, new DateTime(2010, 10, 10), new DateTime(), false, false));

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            this.autoMocker.GetMock<IMapper>()
                .Setup(x => x.Map<Entry, EntryDTO>(stubEntry))
                .Returns(new EntryDTO() { Id = 1, Gameweek = 1 });

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(new Guid(), "John", "Test"));

            var sut = this.autoMocker.CreateInstance<EntryController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            await Assert.ThrowsAsync<TeamNotPickedException>(async () => await sut.GetByGameweek(teamId, 1));
        }

        [Fact]
        public async Task UpdateReturnsCorrectResults()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var stubRequest = new UpdateEntryRequest() {  Players = new List<EntryPlayer>() };
            var stubGuid = Guid.NewGuid();
            var stubUser = new User(stubGuid, "First", "Second");

            this.autoMocker.Use(stubSettings);

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(stubUser);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork.Setup(x => x.EntryRepository(stubGuid))
               .Returns(this.autoMocker.GetMock<IEntryRepository>().Object);
            mockUnitOfWork.Setup(x => x.MessageRepository)
               .Returns(this.autoMocker.GetMock<IMessageRepository>().Object);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<EntryController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            var result = await sut.Update(stubGuid, 1, stubRequest) as StatusCodeResult;

            mockUnitOfWork
                .Verify(x => x.EntryRepository(stubGuid).Update(It.IsAny<Entry>()), Times.Once);
            mockUnitOfWork
                .Verify(x => x.MessageRepository.Insert(It.IsAny<Message>()), Times.Once);
            this.autoMocker.GetMock<IUpdateEntryRequestVerifier>()
                .Verify(x => x.Verify(mockUnitOfWork.Object, stubRequest, stubUser, 1), Times.Once);

            Assert.Equal(200, result.StatusCode);
        }
    }
}
