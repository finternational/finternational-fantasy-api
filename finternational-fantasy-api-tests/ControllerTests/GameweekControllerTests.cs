﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Controllers;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.ControllerTests
{
    public class GameweekControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResults()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var gameweeks = new List<Gameweek>() { new Gameweek(1, true, new DateTime(), new DateTime(), false, false), new Gameweek(1, true, new DateTime(), new DateTime(), false, false) };
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.GameweekRepository.Get(1000, 1))
                .ReturnsAsync(gameweeks);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<GameweekController>();
            var result = await sut.Get();

            Assert.Equal(gameweeks, result);
        }
    }
}
