﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Controllers;
using finternational_fantasy_api.DTOs.Request;
using finternational_fantasy_api.Exceptions;
using finternational_fantasy_api.Security;
using finternational_fantasy_api_tests.Helpers;
using Moq;
using Moq.AutoMock;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.ControllerTests
{
    public class LeagueControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResults()
        {
            var userId = Guid.NewGuid();

            var stubSettings = new PersistenceSettings("connectionstring");
            var league = new League(1, "League", true, 1, Guid.Empty, userId, true);
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(userId, "John", "Test"));

            mockUnitOfWork
                .Setup(x => x.LeagueRepository.GetById(1))
                .ReturnsAsync(league);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();
            var result = await sut.Get(1);

            Assert.Equal(league, result);
        }

        [Fact]
        public async Task GetReturnsThrowsExceptionIfUserIsNotCreator()
        {
            var userId = Guid.NewGuid();

            var stubSettings = new PersistenceSettings("connectionstring");
            var league = new League(1, "League", true, 1, Guid.Empty, userId, true);
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(Guid.NewGuid(), "John", "Test"));

            mockUnitOfWork
                .Setup(x => x.LeagueRepository.GetById(1))
                .ReturnsAsync(league);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            await Assert.ThrowsAsync<ForbiddenRequestException>(async () => { await sut.Get(1); });
        }

        [Fact]
        public async Task CreateReturnsCorrectResults()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var userId = Guid.NewGuid();
            var league = new League(1, "League", true, 1, Guid.Empty, Guid.Empty, true);
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.LeagueRepository.Create(It.IsAny<League>())) //spy and check this is correctly formed object being passed in
                .ReturnsAsync(league);

            mockUnitOfWork
                .Setup(x => x.TeamRepository.GetById(userId))
                .ReturnsAsync(new Team(userId, "Hello", "John", "Test", null));

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(userId, "John", "Test"));

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            var result = await sut.Create(new CreateLeagueRequest("League", true, 1));

            mockUnitOfWork
                .Verify(x => x.TeamRepository.Update(userId, "Hello", 1), Times.Once());
            mockUnitOfWork
                .Verify(x => x.Save(), Times.Once());

            Assert.Equal(league, result);
        }

        [Fact]
        public async Task UpdateReturnsCorrectResults()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var userId = Guid.NewGuid();
            var league = new League(1, "League", false, 1, Guid.Empty, userId, false);
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.LeagueRepository.GetById(1))
                .ReturnsAsync(league);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(userId, "John", "Test"));

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            var result = await sut.Update(1, new UpdateLeagueRequest("League", true, 1));

            mockUnitOfWork
                .Verify(x => x.Save(), Times.Once());
            mockUnitOfWork
                .Verify(x => x.LeagueRepository.Update(It.IsAny<League>()), Times.Once());//spy and check this is correctly formed object being passed in
        }


        [Fact]
        public async Task UpdateThrowsForbiddenRequestExceptionIfUserDidNotCreateLeague()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var userId = Guid.NewGuid();
            var league = new League(1, "League", false, 1, Guid.Empty, Guid.NewGuid(), false);
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.LeagueRepository.GetById(1)) //spy and check this is correctly formed object being passed in
                .ReturnsAsync(league);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(userId, "John", "Test"));

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            await Assert.ThrowsAsync<ForbiddenRequestException>(async () => await sut.Update(1, new UpdateLeagueRequest("League", true, 1)));           
        }

        [Fact]
        public async Task UpdateThrowsInvalidRequestExceptionIfPlayersAlreadyPicked()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var userId = Guid.NewGuid();
            var league = new League(1, "League", false, 1, Guid.Empty, userId, true);
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.LeagueRepository.GetById(1)) //spy and check this is correctly formed object being passed in
                .ReturnsAsync(league);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(userId, "John", "Test"));

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            await Assert.ThrowsAsync<InvalidRequestException>(async () => await sut.Update(1, new UpdateLeagueRequest("League", true, 1)));
        }

        [Fact]
        public async Task JoinReturnsCorrectResult()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var userId = Guid.NewGuid();
            var joinCode = Guid.NewGuid();
            var league = new League(1, "League", false, 1, Guid.Empty, userId, false);
            var team = new Team(userId, "the team", "a", "b", null);
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.LeagueRepository.GetByJoinCode(joinCode))
                .ReturnsAsync(league);
            mockUnitOfWork
                .Setup(x => x.TeamRepository.GetById(userId))
                .ReturnsAsync(team);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(userId, "John", "Test"));

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            var result = await sut.Join(new JoinLeagueRequest(joinCode));

            mockUnitOfWork
                .Verify(x => x.Save(), Times.Once());
            mockUnitOfWork
               .Verify(x => x.TeamRepository.Update(userId, team.TeamName, league.Id), Times.Once());

            Assert.Equal(league.Id, result.Id);
        }
    }
}
