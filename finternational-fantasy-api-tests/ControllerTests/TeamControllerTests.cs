using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Controllers;
using finternational_fantasy_api.Exceptions;
using finternational_fantasy_api.Requests;
using finternational_fantasy_api.Security;
using finternational_fantasy_api_tests.Helpers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.AutoMock;
using System;
using System.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.ControllerTests
{
    public class TeamControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResults()
        {
            var id = Guid.NewGuid();
            var stubSettings = new PersistenceSettings("connectionstring");

            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x=> x.TeamRepository.GetById(id))
                .ReturnsAsync(new Team(id, "Team", "Test", "Tester", null));
            
            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<TeamController>();
            var result = await sut.GetById(id);

            Assert.Equal("Team", result.TeamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("a huge string over 30 characters as 30 characters is the limit of the team name")]
        public async Task UpdateThrowsExceptionIfTeamNameIsInvalid(string teamName)
        {
            var request = new UpdateTeamRequest(teamName);
            var stubSettings = new PersistenceSettings("connectionstring");

            this.autoMocker.Use(stubSettings);

            //look into refactoring this into creating own mocks, will mean more reusable code
            var sut = this.autoMocker.CreateInstance<TeamController>();

            await Assert.ThrowsAsync<TeamNameInvalidException>(async () => await sut.Update(request, Guid.NewGuid()));
        }

        [Fact]
        public async Task UpdateThrowsExceptionIfUserIsTryingToUpdateADifferentTeam()
        {
            var request = new UpdateTeamRequest("team");
            var stubSettings = new PersistenceSettings("connectionstring");

            this.autoMocker.Use(stubSettings);

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(Guid.NewGuid(), "First", "Second"));

            var sut = this.autoMocker.CreateInstance<TeamController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            await Assert.ThrowsAsync<SecurityException>(async () => await sut.Update(request, Guid.NewGuid()));
        }

        [Fact]
        public async Task UpdateReturnsOkIfRequestIsValid()
        {
            var request = new UpdateTeamRequest("team");
            var id = Guid.NewGuid();
            var stubSettings = new PersistenceSettings("connectionstring");

            this.autoMocker.Use(stubSettings);

            this.autoMocker.GetMock<IUserGetter>()
                .Setup(x => x.Get(It.IsAny<ClaimsIdentity>()))
                .Returns(new User(id, "First", "Second"));

            var mockTeamRepo = this.autoMocker.GetMock<ITeamRepository>();

            mockTeamRepo.Setup(x => x.GetById(It.IsAny<Guid>()))
                .ReturnsAsync(new Team(id, "team1", "John", "Test", 1));

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.TeamRepository)
                .Returns(mockTeamRepo.Object);        

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<TeamController>();
            sut.ControllerContext = ControllerContextHelper.GetControllerContext();

            var result = await sut.Update(request, id) as StatusCodeResult;

            Assert.Equal(200, result.StatusCode);
        }
    }
}