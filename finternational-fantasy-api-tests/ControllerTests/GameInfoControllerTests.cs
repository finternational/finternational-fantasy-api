﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Controllers;
using Moq;
using Moq.AutoMock;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.ControllerTests
{
    public class GameInfoControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetByGameIdReturnsCorrectResults()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var gameInfo = new GameInfo() { GameId = 1 };
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.GameInfoRepository.GetByGameId(1))
                .ReturnsAsync(gameInfo);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<GameInfoController>();
            var result = await sut.GetByGameId(1);

            Assert.Equal(gameInfo, result);
        }
    }
}
