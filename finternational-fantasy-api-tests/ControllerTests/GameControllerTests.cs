﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Controllers;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.ControllerTests
{
    public class GameControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetByGameweekReturnsCorrectResults()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var games = new List<Game>() { new Game(1, 1, "France", 0, 0, "England", 3, new DateTime(), true, new DateTime(), new DateTime(),  true) };
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.GameRepository.GetByGameweek(1))
                .ReturnsAsync(games);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<GameController>();
            var result = await sut.GetByGameweek(1);

            Assert.Equal(games, result);
        }

        [Fact]
        public async Task GetTodaysGamesReturnsCorrectResults()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var games = new List<Game>() { new Game(1, 1, "France", 0, 0, "England", 3, new DateTime(), true, new DateTime(), new DateTime(), true) };
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.GameRepository.GetTodaysGames())
                .ReturnsAsync(games);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<GameController>();
            var result = await sut.GetTodaysGames();

            Assert.Equal(games, result);
        }
    }
}
