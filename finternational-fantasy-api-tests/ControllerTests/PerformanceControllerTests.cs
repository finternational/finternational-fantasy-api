﻿using AutoMapper;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Controllers;
using finternational_fantasy_api.DTOs;
using finternational_fantasy_api.Mapping;
using Moq;
using Moq.AutoMock;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.ControllerTests
{
    public class PerformanceControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResults()
        {
            var stubSettings = new PersistenceSettings("connectionstring");
            var performanceDto = new List<PerformanceDTO>() { new PerformanceDTO() { Goals = 1 } };

            var performances = new List<ExtendedPerformance>();
            var events = new List<Event>();

            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.PerformanceRepository.GetByPlayerId(1))
                .ReturnsAsync(performances);

            mockUnitOfWork
                .Setup(x => x.EventRepository.GetByPlayerId(1))
                .ReturnsAsync(events);

            this.autoMocker.GetMock<IMapper>()
                .Setup(x => x.Map<List<ExtendedPerformance>, List<PerformanceDTO>>(performances))
                .Returns(performanceDto);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<PerformanceController>();
            var result = await sut.GetByPlayerId(1);

            Assert.Equal(performanceDto, result);
        }
    }
}
