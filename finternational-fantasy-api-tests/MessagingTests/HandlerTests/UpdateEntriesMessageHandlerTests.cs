﻿using finternational_common.Entities;
using finternational_common.Enums;
using finternational_common.Messaging;
using Moq.AutoMock;
using System.Text.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using Moq;
using finternational_fantasy_api.Messaging.Handlers;

namespace finternational_fantasy_api_tests.MessagingTests.HandlerTests
{
    public class UpdateEntriesMessageHandlerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task HandleMessageHandlesMessageCorrectly()
        {
            var stubEntry = new Entry() { Players = new List<EntryPlayer>(), TeamId = Guid.NewGuid() };
            var stubMessage = new FinternationalMessage(MessageTypeEnum.UpdateEntries, JsonSerializer.Serialize(stubEntry), 1);

            var stubSettings = new PersistenceSettings("connectionstring");
            this.autoMocker.Use(stubSettings);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.EntryRepository(stubEntry.TeamId).GetUpcomingEntryIds())
                .ReturnsAsync(new List<int>() { 1,2 });

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<UpdateEntriesMessageHandler>();

            await sut.HandleMessage(stubMessage);

            this.autoMocker.GetMock<IUnitOfWork>()
                .Verify(x => x.EntryRepository(stubEntry.TeamId).Update(It.IsAny<Entry>()), Times.Exactly(2));
            this.autoMocker.GetMock<IUnitOfWork>()
                .Verify(x => x.Save(), Times.Once);
        }
    }
}
