﻿using finternational_common.Enums;
using finternational_common.Messaging;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Messaging.Handlers;
using Moq;
using Moq.AutoMock;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.MessagingTests.HandlerTests
{
    public class InsertPerformancesMessageHandlerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task HandleMessageHandlesMessageCorrectly()
        {
            var insertPerformanceMessage = new InsertPerformancesMessage() {  HomeTeam = 1, AwayTeam = 2, GameId = 3 };
            var stubMessage = new FinternationalMessage(MessageTypeEnum.InsertPerformances, JsonSerializer.Serialize(insertPerformanceMessage), 1);
            var stubSettings = new PersistenceSettings("constring");

            this.autoMocker.Use(stubSettings);
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.PerformanceRepository)
                .Returns(this.autoMocker.GetMock<IPerformanceRepository>().Object);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<InsertPerformancesMessageHandler>();

            await sut.HandleMessage(stubMessage);

            mockUnitOfWork
                .Verify(x => x.PerformanceRepository.InsertPerformancesForGame(3, 1, 2));
            mockUnitOfWork
                .Verify(x => x.Save(), Times.Once);
        }
    }
}
