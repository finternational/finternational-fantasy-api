﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using finternational_fantasy_api.Exceptions;
using finternational_fantasy_api.Requests;
using finternational_fantasy_api.Security;
using finternational_fantasy_api.Verification;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Security;
using System.Threading.Tasks;
using Xunit;

namespace finternational_fantasy_api_tests.VerificationTests
{
    public class UpdateEntryRequestVerifierTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();
        private readonly UpdateEntryRequestVerifier sut;
        private readonly Mock<IUnitOfWork> unitOfWork;
        private readonly Guid teamId = Guid.NewGuid();
        private readonly User user;


        public UpdateEntryRequestVerifierTests()
        {
            this.user =  new User(teamId, "John", "Test");

            this.unitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            var gameweekRepo = this.autoMocker.GetMock<IGameweekRepository>();
            var entryRepo = this.autoMocker.GetMock<IEntryRepository>();
            var playerRepo = this.autoMocker.GetMock<IPlayerRepository>();

            var players = new List<Player>()
            {
                new Player(1, "Dean", "Henderson", 1, new Country(1, "England"), "", 2), new Player(2, "Iker", "Casillas", 1, new Country(2, "Spain"), "", 2), new Player(3, "Hugo", "Lloris", 1, new Country(3, "France"), "", 2),
                new Player(4, "Roberto", "Carlos", 2, new Country(4, "Brazil"), "", 2), new Player(5, "Lillian", "Thuram", 2, new Country(3, "France"),"", 2), new Player(6, "John", "Terry", 2, new Country(1, "England"), "", 2),
                new Player(7, "Gerard", "Pique", 2, new Country(2, "Spain"), "", 2), new Player(8, "Virgil", "van Dijk", 2, new Country(5, "Belgium"), "", 2), new Player(9, "Ruben", "Dias", 2, new Country(6, "Portugal"), "", 2),
                new Player(10, "Aaron", "Ramsey", 3, new Country(7, "Wales"), "", 2), new Player(11, "Luka", "Modric", 3, new Country(8, "Croatia"), "", 2), new Player(12, "Toni", "Kroos", 3, new Country(9, "Germany"), "", 2),
                new Player(13, "Wayne", "Rooney", 4, new Country(1, "England"), "", 2), new Player(14, "Lukas", "Podolski", 4, new Country(9, "Germany"), "", 2), new Player(15, "Gareth", "Bale", 4, new Country(7, "Wales"), "", 2),
                new Player(16, "Paul", "Pogba", 3, new Country(3, "France"), "", 2), new Player(17, "Bruno", "Fernandes", 3, new Country(6, "Portugal"), "", 2), new Player(18, "Rio", "Ferdinand", 2, new Country(1, "England"), "", 2),
                new Player(19, "Lionel", "Messi", 4, new Country(10, "Argentina"), "", 2), new Player(20, "Peter", "Crouch", 4, new Country(1, "England"), "", 2), new Player(21, "Frank", "Lampard", 3, new Country(1, "England"), "", 2)
            };

            gameweekRepo.Setup(x => x.GetByEntryId(1)).ReturnsAsync(new Gameweek(1, false, new DateTime(9999, 10, 10), new DateTime(9999, 11, 11), true, false));
            gameweekRepo.Setup(x => x.GetByEntryId(3)).ReturnsAsync(new Gameweek(1, false, new DateTime(2001, 10, 10), new DateTime(2001, 11, 11), false, false));
            
            entryRepo.Setup(x => x.GetById(1)).ReturnsAsync(new Entry() { TeamId = teamId });
            entryRepo.Setup(x => x.GetById(2)).ReturnsAsync(new Entry() { TeamId = Guid.NewGuid() });
            entryRepo.Setup(x => x.GetById(3)).ReturnsAsync(new Entry() { TeamId = teamId });

            playerRepo.Setup(x => x.FindByIds(It.IsAny<List<int>>())).ReturnsAsync(players);

            unitOfWork.Setup(x => x.GameweekRepository).Returns(gameweekRepo.Object);
            unitOfWork.Setup(x => x.EntryRepository(It.IsAny<Guid>())).Returns(entryRepo.Object);
            unitOfWork.Setup(x => x.PlayerRepository).Returns(playerRepo.Object);

            this.sut = this.autoMocker.CreateInstance<UpdateEntryRequestVerifier>();
        }

        [Fact]
        public async Task VerifyThrowsSecurityExceptionIfUserIdDoesNotMatchTeamId()
        {
            var request = new UpdateEntryRequest() { Players = new List<EntryPlayer>() };

            await Assert.ThrowsAsync<SecurityException>(async () => await sut.Verify(unitOfWork.Object, request, user, 2));
        }

        [Fact]
        public async Task VerifyThrowsDeadlinePassedExceptionifDeadlineHasPassed()
        {
            var request = new UpdateEntryRequest() { Players = new List<EntryPlayer>() };

            await Assert.ThrowsAsync<DeadlinePassedException>(async () => await sut.Verify(unitOfWork.Object, request, user, 3));
        }

        [Fact]
        public async Task VerifyThrowsInvalidRequestExceptionIfPlayersIsnt15()
        {
            var request = new UpdateEntryRequest()
            { 
                Players = new List<EntryPlayer>()
                {
                    new EntryPlayer(true, true, true, 1, 11), new EntryPlayer(true, true, true, 2, 1),
                    new EntryPlayer(true, true, true, 3, 12), new EntryPlayer(true, true, true, 4, 2),
                    new EntryPlayer(true, true, true, 5, 13), new EntryPlayer(true, true, true, 6, 3),
                    new EntryPlayer(true, true, true, 7, 14), new EntryPlayer(true, true, true, 14, 4),
                    new EntryPlayer(true, true, true, 8, 15), new EntryPlayer(true, true, true, 9999, 5),
                    new EntryPlayer(true, true, true, 9, 6),
                    new EntryPlayer(true, true, true, 10, 7),
                    new EntryPlayer(true, true, true, 11, 8),
                    new EntryPlayer(true, true, true, 12, 9),
                    new EntryPlayer(true, true, true, 13, 10),

                }
            };

            var exception = await Assert.ThrowsAsync<InvalidRequestException>(async () => await sut.Verify(unitOfWork.Object, request, user, 1));

            Assert.Equal("One or more players is not valid", exception.Message);
        }

        [Fact]
        public async Task VerifyThrowsInvalidRequestExceptionIfSquadHasTooManyGoalkeepers()
        {
            var request = new UpdateEntryRequest()
            {
                Players = new List<EntryPlayer>()
                {
                    new EntryPlayer(true, true, true, 1, 1), new EntryPlayer(true, true, true, 2, 3),
                    new EntryPlayer(true, true, true, 3, 2), new EntryPlayer(true, true, true, 4, 4),
                    new EntryPlayer(true, true, true, 5, 5), new EntryPlayer(true, true, true, 6, 6),
                    new EntryPlayer(true, true, true, 7, 7), new EntryPlayer(true, true, true, 14, 14),
                    new EntryPlayer(true, true, true, 8, 8), new EntryPlayer(true, true, true, 15, 15),
                    new EntryPlayer(true, true, true, 9, 9),
                    new EntryPlayer(true, true, true, 10, 10),
                    new EntryPlayer(true, true, true, 11, 11),
                    new EntryPlayer(true, true, true, 12, 12),
                    new EntryPlayer(true, true, true, 13, 13),

                }
            };

            var exception = await Assert.ThrowsAsync<InvalidRequestException>(async () => await sut.Verify(unitOfWork.Object, request, user, 1));

            Assert.Equal("Incorrect number of goalkeepers in squad", exception.Message);
        }

        [Fact]
        public async Task VerifyThrowsInvalidRequestExceptionIfSquadHasTooManyDefenders()
        {
            var request = new UpdateEntryRequest()
            {
                Players = new List<EntryPlayer>()
                {
                    new EntryPlayer(true, true, true, 1, 1), new EntryPlayer(true, true, true, 18, 11),
                    new EntryPlayer(true, true, true, 3, 2), new EntryPlayer(true, true, true, 4, 12),
                    new EntryPlayer(true, true, true, 5, 3), new EntryPlayer(true, true, true, 6, 13),
                    new EntryPlayer(true, true, true, 7, 4), new EntryPlayer(true, true, true, 14, 14),
                    new EntryPlayer(true, true, true, 8, 5), new EntryPlayer(true, true, true, 15, 1),
                    new EntryPlayer(true, true, true, 9, 6),
                    new EntryPlayer(true, true, true, 10, 7),
                    new EntryPlayer(true, true, true, 11, 8),
                    new EntryPlayer(true, true, true, 12, 9),
                    new EntryPlayer(true, true, true, 13, 10),

                }
            };

            var exception = await Assert.ThrowsAsync<InvalidRequestException>(async () => await sut.Verify(unitOfWork.Object, request, user, 1));

            Assert.Equal("Incorrect number of defenders in squad", exception.Message);
        }

        [Fact]
        public async Task VerifyThrowsInvalidRequestExceptionIfSquadHasTooManyMidfielders()
        {
            var request = new UpdateEntryRequest()
            {
                Players = new List<EntryPlayer>()
                {
                    new EntryPlayer(true, true, true, 1, 1), new EntryPlayer(true, true, true, 17, 1),
                    new EntryPlayer(true, true, true, 3, 1), new EntryPlayer(true, true, true, 4, 1),
                    new EntryPlayer(true, true, true, 5, 1), new EntryPlayer(true, true, true, 6, 1),
                    new EntryPlayer(true, true, true, 7, 1), new EntryPlayer(true, true, true, 16, 1),
                    new EntryPlayer(true, true, true, 8, 1), new EntryPlayer(true, true, true, 15, 1),
                    new EntryPlayer(true, true, true, 21, 1),
                    new EntryPlayer(true, true, true, 10, 1),
                    new EntryPlayer(true, true, true, 11, 1),
                    new EntryPlayer(true, true, true, 12, 1),
                    new EntryPlayer(true, true, true, 13, 1),

                }
            };

            var exception = await Assert.ThrowsAsync<InvalidRequestException>(async () => await sut.Verify(unitOfWork.Object, request, user, 1));

            Assert.Equal("Incorrect number of midfielders in squad", exception.Message);
        }

        [Fact]
        public async Task VerifyThrowsInvalidRequestExceptionIfSquadHasTooManyFromOneCountry()
        {
            var request = new UpdateEntryRequest()
            {
                Players = new List<EntryPlayer>()
                {
                    new EntryPlayer(true, true, true, 1, 1), new EntryPlayer(true, true, true, 17, 1),
                    new EntryPlayer(true, true, true, 3, 2), new EntryPlayer(true, true, true, 4, 4),
                    new EntryPlayer(true, true, true, 5, 3), new EntryPlayer(true, true, true, 6, 5),
                    new EntryPlayer(true, true, true, 7, 1), new EntryPlayer(true, true, true, 16, 1),
                    new EntryPlayer(true, true, true, 8, 1), new EntryPlayer(true, true, true, 15, 1),
                    new EntryPlayer(true, true, true, 20, 1),
                    new EntryPlayer(true, true, true, 10, 1),
                    new EntryPlayer(true, true, true, 11, 1),
                    new EntryPlayer(true, true, true, 12, 1),
                    new EntryPlayer(true, true, true, 13, 1),

                }
            };

            var exception = await Assert.ThrowsAsync<InvalidRequestException>(async () => await sut.Verify(unitOfWork.Object, request, user, 1));

            Assert.Equal("Too many players from one country", exception.Message);
        }

        [Fact]
        public async Task VerifyThrowsInvalidRequestExceptionIfTeamHasToomanyGoalkeepers()
        {
            var request = new UpdateEntryRequest()
            {
                Players = new List<EntryPlayer>()
                {
                    new EntryPlayer(true, true, true, 1, 1), new EntryPlayer(true, true, true, 17, 1),
                    new EntryPlayer(true, true, true, 3, 1), new EntryPlayer(true, true, true, 4, 1),
                    new EntryPlayer(true, true, true, 5, 1), new EntryPlayer(true, true, true, 6, 1),
                    new EntryPlayer(true, true, true, 7, 1), new EntryPlayer(true, true, true, 16, 1),
                    new EntryPlayer(true, true, true, 8, 1), new EntryPlayer(true, true, true, 15, 1),
                    new EntryPlayer(true, true, true, 19, 1),
                    new EntryPlayer(true, true, true, 10, 1),
                    new EntryPlayer(true, true, true, 11, 1),
                    new EntryPlayer(true, true, true, 12, 1),
                    new EntryPlayer(true, true, true, 13, 1),

                }
            };

            var exception = await Assert.ThrowsAsync<InvalidRequestException>(async () => await sut.Verify(unitOfWork.Object, request, user, 1));

            Assert.Equal("Incorrect number of goalkeepers in team", exception.Message);
        }

        [Fact]
        public async Task VerifyThrowsInvalidRequestExceptionIfTeamHasIncorrectNumberOfDefenders()
        {
            var request = new UpdateEntryRequest()
            {
                Players = new List<EntryPlayer>()
                {
                    new EntryPlayer(true, true, true, 1, 1), new EntryPlayer(true, true, true, 17, 1),
                    new EntryPlayer(true, true, false, 3, 1), new EntryPlayer(true, true, true, 4, 1),
                    new EntryPlayer(true, true, true, 5, 1), new EntryPlayer(true, true, true, 6, 1),
                    new EntryPlayer(true, true, true, 7, 1), new EntryPlayer(true, true, true, 16, 1),
                    new EntryPlayer(true, true, true, 8, 1), new EntryPlayer(true, true, true, 15, 1),
                    new EntryPlayer(true, true, true, 19, 1),
                    new EntryPlayer(true, true, true, 10, 1),
                    new EntryPlayer(true, true, true, 11, 1),
                    new EntryPlayer(true, true, true, 12, 1),
                    new EntryPlayer(true, true, true, 13, 1),

                }
            };

            var exception = await Assert.ThrowsAsync<InvalidRequestException>(async () => await sut.Verify(unitOfWork.Object, request, user, 1));

            Assert.Equal("Incorrect number of defenders in team", exception.Message);
        }

        [Fact]
        public async Task VerifyThrowsInvalidRequestExceptionIfTeamHasIncorrectNumberOfMidfielders()
        {
            var request = new UpdateEntryRequest()
            {
                Players = new List<EntryPlayer>()
                {
                    new EntryPlayer(true, true, true, 1, 1), new EntryPlayer(true, true, true, 17, 1),
                    new EntryPlayer(true, true, false, 3, 1), new EntryPlayer(true, true, false, 4, 1),
                    new EntryPlayer(true, true, false, 5, 1), new EntryPlayer(true, true, false, 6, 1),
                    new EntryPlayer(true, true, true, 7, 1), new EntryPlayer(true, true, true, 16, 1),
                    new EntryPlayer(true, true, true, 8, 1), new EntryPlayer(true, true, true, 15, 1),
                    new EntryPlayer(true, true, true, 19, 1),
                    new EntryPlayer(true, true, true, 10, 1),
                    new EntryPlayer(true, true, true, 11, 1),
                    new EntryPlayer(true, true, true, 12, 1),
                    new EntryPlayer(true, true, true, 13, 1),

                }
            };

            var exception = await Assert.ThrowsAsync<InvalidRequestException>(async () => await sut.Verify(unitOfWork.Object, request, user, 1));

            Assert.Equal("Incorrect number of midfielders in team", exception.Message);
        }
    }
}
 
