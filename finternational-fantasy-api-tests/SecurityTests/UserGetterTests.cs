﻿using finternational_fantasy_api.Security;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Security;
using System.Security.Claims;
using Xunit;

namespace finternational_fantasy_api_tests.SecurityTests
{
    public class UserGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void GetThrowsSecurityExceptionIfAClaimDoesntExist()
        {
            var claims = new ClaimsIdentity(new List<Claim>());

            var sut = this.autoMocker.CreateInstance<UserGetter>();

            Assert.Throws<SecurityException>(() => sut.Get(claims));
        }

        [Fact]
        public void GetReturnsCorrectResults()
        {
            var claims = new List<Claim>()
            {
                new Claim(type: ClaimTypes.NameIdentifier, value: Guid.NewGuid().ToString()),
                new Claim(type: ClaimTypes.GivenName, value: "John"),
                new Claim(type: ClaimTypes.Surname, value: "McTest"),
            };

            var claimsIdentity = new ClaimsIdentity(claims);

            var sut = this.autoMocker.CreateInstance<UserGetter>();
            var result = sut.Get(claimsIdentity);

            Assert.Equal(Guid.Parse(claims[0].Value), result.Id);
            Assert.Equal(claims[1].Value, result.FirstName);
            Assert.Equal(claims[2].Value, result.SecondName);
        }
    }
}
