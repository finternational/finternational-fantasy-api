﻿namespace finternational_fantasy_api.Exceptions
{
    public class InvalidRequestException : Exception
    {
        public InvalidRequestException() { }
        public InvalidRequestException(string message) : base(message) { }
    }
}
