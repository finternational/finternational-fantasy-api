﻿using finternational_common.Persistence;
using finternational_common.Persistence.Interfaces;

namespace finternational_fantasy_api.Configuration
{
    public static class PersistenceConfigurationExtensions
    {
        public static void AddPersistenceLayer(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWorkProvider, UnitOfWorkProvider>();
            services.AddTransient<IConnectionProvider, ConnectionProvider>();
        }
    }
}
