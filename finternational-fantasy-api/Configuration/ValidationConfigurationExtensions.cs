﻿using finternational_fantasy_api.DTOs.Request;
using finternational_fantasy_api.Requests;
using finternational_fantasy_api.Validation.Validators;
using FluentValidation;

namespace finternational_fantasy_api.Configuration
{
    public static class ValidationConfigurationExtensions
    {
        public static void AddValidators(this IServiceCollection services)
        {
            services.AddTransient<IValidator<UpdateEntryRequest>, UpdateEntryRequestValidator>();
            services.AddTransient<IValidator<CreateLeagueRequest>, CreateLeagueRequestValidator>();
            services.AddTransient<IValidator<UpdateLeagueRequest>, UpdateLeagueRequestValidator>();
        }
    }
}
