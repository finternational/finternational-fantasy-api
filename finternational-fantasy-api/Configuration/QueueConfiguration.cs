﻿namespace finternational_fantasy_api.Configuration
{
    public record QueueSettings(string ConnectionString, string QueueName) { }
    public static class QueueConfiguration
    {
        public static void RegisterQueueSettings(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton(new QueueSettings(
                config.GetValue<string>("Queue:ConnectionString"),
                config.GetValue<string>("Queue:QueueName")));
        }
    }

}
