﻿using finternational_fantasy_api.Messaging.Handlers;

namespace finternational_fantasy_api.Configuration
{
    public static class MessagingConfigurationExtensions
    {
        public static void AddMessagingLayer(this IServiceCollection services)
        {
            services.AddTransient<UpdateEntriesMessageHandler>();
            services.AddTransient<InsertPerformancesMessageHandler>();
        }
    }
}
