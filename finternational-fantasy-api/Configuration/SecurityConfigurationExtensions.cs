﻿using finternational_fantasy_api.Security;

namespace finternational_fantasy_api.Configuration
{
    public static class SecurityConfigurationExtensions
    {
        public static void AddSecurityLayer(this IServiceCollection services)
        {
            services.AddTransient<IUserGetter, UserGetter>();
        }
    }
}
