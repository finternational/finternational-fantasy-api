﻿using finternational_fantasy_api.Verification;

namespace finternational_fantasy_api.Configuration
{
    public static class VerificationConfigurationExtensions
    {
        public static void AddVerification(this IServiceCollection services)
        {
            services.AddTransient<IUpdateEntryRequestVerifier, UpdateEntryRequestVerifier>();
        }
    }
}
