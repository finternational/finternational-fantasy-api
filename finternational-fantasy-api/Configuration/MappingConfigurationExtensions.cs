﻿using AutoMapper;
using finternational_fantasy_api.Mapping;

namespace finternational_fantasy_api.Configuration
{
    public static class MappingConfigurationExtensions
    {
        public static void AddMapping(this IServiceCollection services)
        {
            MapperConfiguration mapperConfig = new MapperConfiguration(x => x.AddMaps("finternational-fantasy-api"));
            mapperConfig.AssertConfigurationIsValid();
            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}