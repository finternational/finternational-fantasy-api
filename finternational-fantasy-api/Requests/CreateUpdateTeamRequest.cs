﻿namespace finternational_fantasy_api.Requests
{
    public record UpdateTeamRequest(string TeamName){}
}
