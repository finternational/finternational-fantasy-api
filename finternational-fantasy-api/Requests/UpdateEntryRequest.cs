﻿using finternational_common.Entities;

namespace finternational_fantasy_api.Requests
{
    public class UpdateEntryRequest
    {
        public List<EntryPlayer> Players { get; set; }
    }
}
