﻿using AutoMapper;
using finternational_common.Entities;
using finternational_fantasy_api.DTOs;

namespace finternational_fantasy_api.Mapping
{
    public class PerformanceProfile : Profile
    {
        public PerformanceProfile()
        {
            CreateMap<ExtendedPerformance, PerformanceDTO>()
                .ForMember(dest => dest.Gameweek, opt => opt.MapFrom(src => src.Gameweek))
                .ForMember(dest => dest.PlayerTeamScore, opt => opt.MapFrom(src => src.PlayerTeamGoals))
                .ForMember(dest => dest.OppositionTeamScore, opt => opt.MapFrom(src => src.OppositionGoals))
                .ForMember(dest => dest.OppositionName, opt => opt.MapFrom(src => src.OppositionName))
                .ForMember(dest => dest.Goals, opt => opt.MapFrom(src => src.Goals))
                .ForMember(dest => dest.Assists, opt => opt.MapFrom(src => src.Assists))
                .ForMember(dest => dest.CleanSheet, opt => opt.MapFrom(src => src.CleanSheet))
                .ForMember(dest => dest.Saves, opt => opt.MapFrom(src => src.Saves))
                .ForMember(dest => dest.PenaltySaves, opt => opt.MapFrom(src => src.PenaltySaved))
                .ForMember(dest => dest.PenaltyMisses, opt => opt.MapFrom(src => src.PenaltyMissed))
                .ForMember(dest => dest.PenaltiesWon, opt => opt.MapFrom(src => src.PenaltyWon))
                .ForMember(dest => dest.Conceded, opt => opt.MapFrom(src => src.Conceded))
                .ForMember(dest => dest.YellowCards, opt => opt.MapFrom(src => src.YellowCard))
                .ForMember(dest => dest.RedCards, opt => opt.MapFrom(src => src.RedCard))
                .ForMember(dest => dest.Minutes, opt => opt.MapFrom(src => src.Minutes))
                .ForMember(dest => dest.OwnGoals, opt => opt.MapFrom(src => src.OwnGoal))
                .ForMember(dest => dest.Points, opt => opt.MapFrom(src => src.Points));
            AllowNullCollections = true;
        }
    }
}
