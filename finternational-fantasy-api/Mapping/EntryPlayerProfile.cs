﻿using AutoMapper;
using finternational_common.Entities;
using finternational_fantasy_api.DTOs;

namespace finternational_fantasy_api.Mapping
{
    public class EntryPlayerProfile : Profile
    {
        public EntryPlayerProfile()
        {
            CreateMap<EntryPlayer, EntryPlayerDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.PlayerId))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.Player.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.Player.LastName))
                .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Player.Country.Name))
                .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.Player.Country.Id))
                .ForMember(dest => dest.ViceCaptain, opt => opt.MapFrom(src => src.ViceCaptain))
                .ForMember(dest => dest.Captain, opt => opt.MapFrom(src => src.Captain))
                .ForMember(dest => dest.Substitute, opt => opt.MapFrom(src => src.Substitute))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Player.Position))
                .ForMember(dest => dest.Opponent, opt => opt.MapFrom(src => src.Opponent))
                .ForMember(dest => dest.Points, opt => opt.MapFrom(src => src.Points))
                .ForMember(dest => dest.SquadPosition, opt => opt.MapFrom(src => src.SquadPosition));
            AllowNullCollections = true;
        }    
    }
}
