﻿using AutoMapper;
using finternational_common.Entities;
using finternational_fantasy_api.DTOs;

namespace finternational_fantasy_api.Mapping
{
    public class EntryProfile : Profile
    {
        public EntryProfile()
        {
            CreateMap<Entry, EntryDTO>()
                .ForMember(dest => dest.Team, opt => opt.MapFrom(src => src.TeamId))
                .ForMember(dest => dest.Gameweek, opt => opt.MapFrom(src => src.GameweekId));
        }
    }
}
