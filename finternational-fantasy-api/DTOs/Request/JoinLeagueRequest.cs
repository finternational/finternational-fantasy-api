﻿namespace finternational_fantasy_api.DTOs.Request
{
    public record JoinLeagueRequest(Guid JoinCode)
    {
    }
}
