﻿namespace finternational_fantasy_api.DTOs.Request
{
    public record UpdateLeagueRequest(string Name, bool Private, int LeagueType)
    {
    }
}
