﻿namespace finternational_fantasy_api.DTOs.Request
{
    public record CreateLeagueRequest(string Name, bool Private, int LeagueType)
    {
    }
}
