﻿namespace finternational_fantasy_api.DTOs
{
    public class EntryDTO
    {
        public int Id { get; set; }
        public Guid Team { get; set; }
        public int Gameweek { get; set; }

        public string TeamName { get; set; }
        public string ManagerFirstName { get; set; }
        public string ManagerlastName { get; set; }
        public List<EntryPlayerDTO> Players { get; set; }
    }
}
