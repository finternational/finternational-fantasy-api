﻿namespace finternational_fantasy_api.DTOs
{
    public class PerformanceDTO 
    {
        public int Gameweek{ get; set; }
        public int? PlayerTeamScore{ get; set; }
        public int? OppositionTeamScore{ get; set; }
        public string? OppositionName{ get; set; }
        public int? Goals{ get; set; }
        public int? Assists{ get; set; }
        public bool? CleanSheet{ get; set; }
        public int? Saves{ get; set; }
        public int? PenaltySaves{ get; set; }
        public int? PenaltyMisses{ get; set; }
        public int? Conceded{ get; set; }
        public int? YellowCards{ get; set; }
        public int? RedCards{ get; set; }
        public int? Minutes{ get; set; }
        public int? OwnGoals{ get; set; }
        public int? PenaltiesWon{ get; set; }
        public int? Points { get; set; }

    }
}
