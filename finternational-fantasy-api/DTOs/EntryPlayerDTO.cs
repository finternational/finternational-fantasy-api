﻿namespace finternational_fantasy_api.DTOs
{
    public class EntryPlayerDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Captain { get; set; }
        public bool ViceCaptain { get; set; }
        public int SquadPosition { get; set; }
        public string CountryName { get; set; }
        public int CountryId { get; set; }
        public bool Substitute { get ; set; }
        public int Position { get; set; }
        public string Opponent { get; set; }
        public int Points { get; set; }
    }
}
