﻿using Azure.Messaging.ServiceBus;
using finternational_common.Enums;
using finternational_common.Messaging;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Messaging.Handlers;
using Newtonsoft.Json;

namespace finternational_fantasy_api.Messaging
{
    public class MessagingService : BackgroundService
    {
        private readonly ILogger<MessagingService> logger;
        private readonly QueueSettings queueSettings;
        private readonly ServiceBusClient client;
        private readonly IServiceProvider serviceProvider;

        public MessagingService(ILogger<MessagingService> logger, QueueSettings queueSettings,
            IServiceProvider serviceProvider)
        {
            this.logger = logger;
            this.queueSettings = queueSettings;
            this.client = new ServiceBusClient(queueSettings.ConnectionString);
            this.serviceProvider = serviceProvider;
        }

        public async Task MessageHandler(ProcessMessageEventArgs args)
        {
            var message = JsonConvert.DeserializeObject<FinternationalMessage>(args.Message.Body.ToString());
            this.logger.LogInformation($"Received message: {args.Message.MessageId} outbox id: {message.OutboxId}");

            try
            {
                var thing = $"finternational_fantasy_api.Messaging.Handlers.{message.Type}MessageHandler"; //move this!

                Type messageHandlerType = Type.GetType(thing);

                var messageHandler = (IMessageHandler)serviceProvider.GetService(messageHandlerType);

                if (messageHandler == null)
                {
                    throw new Exception("Message Handler does not exist for message"); //create an exception for this
                }

                await messageHandler.HandleMessage(message);
                this.logger.LogInformation($"Handled message: {args.Message.MessageId} outbox id: {message.OutboxId}");

                await args.CompleteMessageAsync(args.Message);
            }
            catch(Exception ex)
            {
                this.logger.LogError(ex.Message);
                await args.DeadLetterMessageAsync(args.Message);
            }
        }

        public Task ErrorHandler(ProcessErrorEventArgs args)
        {
            Console.WriteLine(args.Exception.ToString());
            return Task.CompletedTask;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var processor = client.CreateProcessor(this.queueSettings.QueueName, new ServiceBusProcessorOptions());
            processor.ProcessMessageAsync += MessageHandler;
            processor.ProcessErrorAsync += ErrorHandler;

            await processor.StartProcessingAsync();

            stoppingToken.Register(async () =>
            {
               await processor.StopProcessingAsync();
               await processor.CloseAsync();
            });
        }
    }
}
