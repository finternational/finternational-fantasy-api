﻿using finternational_common.Messaging;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using Newtonsoft.Json;

namespace finternational_fantasy_api.Messaging.Handlers
{
    public class InsertPerformancesMessageHandler : IMessageHandler {

        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;

        public InsertPerformancesMessageHandler(IUnitOfWorkProvider unitOfWorkProvider, PersistenceSettings persistenceSettings)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
        }

        public async Task HandleMessage(FinternationalMessage message)
        {
            var typedMessage = JsonConvert.DeserializeObject<InsertPerformancesMessage>(message.Data);

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                await unitOfWork.PerformanceRepository.InsertPerformancesForGame(typedMessage.GameId, typedMessage.HomeTeam, typedMessage.AwayTeam);
                await unitOfWork.Save();
            }
        }
    }
}
