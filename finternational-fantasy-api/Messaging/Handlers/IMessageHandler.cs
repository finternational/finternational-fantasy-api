﻿using finternational_common.Messaging;

namespace finternational_fantasy_api.Messaging.Handlers
{
    public interface IMessageHandler
    {
        Task HandleMessage(FinternationalMessage message);
    }
}
