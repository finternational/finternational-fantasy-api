﻿using finternational_common.Entities;
using finternational_common.Messaging;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using Newtonsoft.Json;

namespace finternational_fantasy_api.Messaging.Handlers
{
    public class UpdateEntriesMessageHandler : IMessageHandler
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;

        public UpdateEntriesMessageHandler(IUnitOfWorkProvider unitOfWorkProvider, PersistenceSettings persistenceSettings)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
        }

        public async Task HandleMessage(FinternationalMessage message)
        {
            var entry = JsonConvert.DeserializeObject<Entry>(message.Data);

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                var ids = await unitOfWork.EntryRepository(entry.TeamId).GetUpcomingEntryIds();

                foreach (var id in ids)
                {
                    entry.Id = id;
                    await unitOfWork.EntryRepository(entry.TeamId).Update(entry);
                }

                await unitOfWork.Save();
            }
        }
    }
}
