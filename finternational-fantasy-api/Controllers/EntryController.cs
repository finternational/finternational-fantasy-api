﻿using AutoMapper;
using finternational_common.Entities;
using finternational_common.Enums;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.DTOs;
using finternational_fantasy_api.Exceptions;
using finternational_fantasy_api.Requests;
using finternational_fantasy_api.Security;
using finternational_fantasy_api.Verification;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Text.Json;

namespace finternational_fantasy_api.Controllers
{
    [ApiController]
    [Route("teams/{teamId:Guid}/entries")]
    public class EntryController : ControllerBase
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;
        private readonly IUserGetter userGetter;
        private readonly IUpdateEntryRequestVerifier updateEntryRequestVerifier;
        private readonly IMapper mapper;

        public EntryController(IUnitOfWorkProvider unitOfWorkProvider, PersistenceSettings persistenceSettings, 
            IUserGetter userGetter, IUpdateEntryRequestVerifier updateEntryRequestVerifier,
            IMapper mapper)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
            this.userGetter = userGetter;
            this.updateEntryRequestVerifier = updateEntryRequestVerifier;
            this.mapper = mapper;
        }

        [HttpGet("gameweek/{gameweekId:int}")]
        public async Task<EntryDTO> GetByGameweek([FromRoute] Guid teamId, [FromRoute]int gameweekId)
        {
            var user = this.userGetter.Get(HttpContext.User.Identity as ClaimsIdentity);

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                var entryTask = unitOfWork.EntryRepository(teamId).GetByGameweekId(gameweekId);
                var gameweekTask = unitOfWork.GameweekRepository.GetById(gameweekId);

                var entry = await entryTask;
                var gameweek = await gameweekTask;

                if (user.Id != teamId)
                {
                    if(gameweek.Deadline > DateTime.UtcNow)
                    {
                        throw new ForbiddenRequestException();
                    }

                    if (entry.Players.Count != 15)
                    {
                        throw new TeamNotPickedException();
                    }
                }

                entry.Players = entry.Players.OrderBy(x => x.SquadPosition).ToList();
                return this.mapper.Map<Entry, EntryDTO>(entry);
            }
        }

        
        [HttpPatch("{id:int}")]
        public async Task<IActionResult> Update([FromRoute] Guid teamId, [FromRoute]int id, [FromBody] UpdateEntryRequest request)
        {
            var user = this.userGetter.Get(HttpContext.User.Identity as ClaimsIdentity);

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                await this.updateEntryRequestVerifier.Verify(unitOfWork, request, user, id);

                var entry = new Entry() { Id = id, Players = request.Players, TeamId = user.Id };

                await unitOfWork.EntryRepository(teamId).Update(entry);

                await unitOfWork.MessageRepository.Insert(new Message(JsonSerializer.Serialize(entry), MessageTypeEnum.UpdateEntries));

                await unitOfWork.Save();
            }

            return Ok();
        }
    }
}
