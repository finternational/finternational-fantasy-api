﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.Exceptions;
using finternational_fantasy_api.Security;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace finternational_fantasy_api.Controllers
{
    [ApiController]
    [Route("league-expanded")]
    public class LeagueExpandedController : ControllerBase
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;
        private readonly IUserGetter userGetter;

        public LeagueExpandedController(
            IUnitOfWorkProvider unitOfWorkProvider, 
            PersistenceSettings persistenceSettings,
            IUserGetter userGetter)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
            this.userGetter = userGetter;
        }

        [HttpGet("{id:int}/gameweek/{gameweek:int}")]
        public async Task<LeagueExpanded> GetById([FromRoute]int id, int gameweek)
        {
            var user = this.userGetter.Get(HttpContext.User.Identity as ClaimsIdentity);

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                var league = await unitOfWork.LeagueRepository.GetById(id);
                var leagueExpanded = await unitOfWork.LeagueExpandedRepository.GetById(id, gameweek);

                if (league.IsPrivate && leagueExpanded.Standings.Where(x => x.TeamId == user.Id).FirstOrDefault() == null)
                {
                    throw new ForbiddenRequestException();
                }

                return leagueExpanded;
            }
        }
    }
}
