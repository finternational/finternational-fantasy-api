using Microsoft.AspNetCore.Mvc;
using finternational_fantasy_api.Requests;
using System.Security.Claims;
using finternational_fantasy_api.Exceptions;
using finternational_fantasy_api.Security;
using System.Security;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_common.Entities;

namespace finternational_fantasy_api.Controllers
{
    [ApiController]
    [Route("teams")]
    public class TeamController : ControllerBase
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly IUserGetter userGetter;
        private readonly PersistenceSettings persistenceSettings;

        public TeamController(IUnitOfWorkProvider unitOfWorkProvider, IUserGetter userGetter, PersistenceSettings persistenceSettings)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.userGetter = userGetter;
            this.persistenceSettings = persistenceSettings;
        }

        [HttpGet("{id:guid}")]
        public async Task<Team> GetById([FromRoute]Guid id)
        {
            using(var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                return await unitOfWork.TeamRepository.GetById(id);
            }
        }

        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> Update([FromBody] UpdateTeamRequest request, [FromRoute]Guid id)
        {
            //Todo: look into validation library
            if (request.TeamName.Length < 1 || request.TeamName.Length > 30)
            {
                throw new TeamNameInvalidException();
            }

            #pragma warning disable CS8604 // Possible null reference argument.
            var user = this.userGetter.Get(HttpContext.User.Identity as ClaimsIdentity);
            #pragma warning restore CS8604 // Possible null reference argument.

            //Todo: admin role?
            if(id != user.Id)
            {
                throw new SecurityException();
            }

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                var team = await unitOfWork.TeamRepository.GetById(id);
                await unitOfWork.TeamRepository.Update(id, request.TeamName, team.League);
                await unitOfWork.Save();
            }

            return Ok();
        }
    }
}