﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using Microsoft.AspNetCore.Mvc;

namespace finternational_fantasy_api.Controllers
{
    [ApiController]
    [Route("games")]
    public class GameController : ControllerBase
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;

        public GameController(IUnitOfWorkProvider unitOfWorkProvider, PersistenceSettings persistenceSettings)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
        }

        [HttpGet("gameweek/{gameweekId:int}")]
        public async Task<List<Game>> GetByGameweek([FromRoute]int gameweekId)
        {
            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                return await unitOfWork.GameRepository.GetByGameweek(gameweekId);
            }
        }

        [HttpGet("today")]
        public async Task<List<Game>> GetTodaysGames()
        {
            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                return await unitOfWork.GameRepository.GetTodaysGames();
            }
        }
    }
}
