﻿using finternational_common.Exceptions;
using finternational_fantasy_api.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Security;

namespace finternational_fantasy_api.Controllers.Filters
{
    public class ExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.Exception is EntityNotFoundException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status404NotFound);
                context.ExceptionHandled = true;
                return;
            }

            if (context.Exception is EntityAlreadyExistsException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status400BadRequest);
                context.ExceptionHandled = true;
                return;
            }

            if (context.Exception is InvalidRequestException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status400BadRequest);
                context.ExceptionHandled = true;
                return;
            }

            if (context.Exception is TeamNameInvalidException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status400BadRequest);
                context.ExceptionHandled = true;
                return;
            }

            if (context.Exception is BadHttpRequestException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status400BadRequest);
                context.ExceptionHandled = true;
                return;
            }

            if (context.Exception is SecurityException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status401Unauthorized);
                context.ExceptionHandled = true;
                return;
            }

            if (context.Exception is InvalidDataException)
            {
                //do we want to have an error message here?
                context.Result = new StatusCodeResult(StatusCodes.Status500InternalServerError);
                context.ExceptionHandled = true;
                return;
            }

            if (context.Exception is DeadlinePassedException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status400BadRequest);
                context.ExceptionHandled = true;
                return;
            }

            if (context.Exception is TeamNotPickedException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status204NoContent);
                context.ExceptionHandled = true;
                return;
            }

            if (context.Exception is ForbiddenRequestException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status403Forbidden);
                context.ExceptionHandled = true;
                return;
            }
        }
    }
}
