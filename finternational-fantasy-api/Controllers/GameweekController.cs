﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using Microsoft.AspNetCore.Mvc;

namespace finternational_fantasy_api.Controllers
{
    [ApiController]
    [Route("gameweeks")]
    public class GameweekController : ControllerBase
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;

        public GameweekController(IUnitOfWorkProvider unitOfWorkProvider, PersistenceSettings persistenceSettings)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
        }

        [HttpGet("")]
        public async Task<List<Gameweek>> Get([FromQuery]int pageSize = 1000, [FromQuery]int pageNumber = 1)
        {
            using(var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                return await unitOfWork.GameweekRepository.Get(pageSize, pageNumber);
            }
        }
    }
}
