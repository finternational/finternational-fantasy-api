﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.DTOs;
using finternational_fantasy_api.DTOs.Request;
using finternational_fantasy_api.Exceptions;
using finternational_fantasy_api.Security;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace finternational_fantasy_api.Controllers
{
    [ApiController]
    [Route("league")]
    public class LeagueController : ControllerBase
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;
        private readonly IUserGetter userGetter;

        public LeagueController(IUnitOfWorkProvider unitOfWorkProvider, PersistenceSettings persistenceSettings, IUserGetter userGetter)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
            this.userGetter = userGetter;
        }

        [HttpGet("{id:int}")]
        public async Task<League> Get([FromRoute]int id)
        {
            var user = this.userGetter.Get(HttpContext.User.Identity as ClaimsIdentity);

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                var league = await unitOfWork.LeagueRepository.GetById(id);

                if (user.Id == league.Creator)
                {
                    return league;
                }

                throw new ForbiddenRequestException();
            }
        }

        [HttpGet("{id:int}/is-admin")]
        public async Task<bool> IsAdmin([FromRoute] int id)
        {
            var user = this.userGetter.Get(HttpContext.User.Identity as ClaimsIdentity);

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                var league = await unitOfWork.LeagueRepository.GetById(id);

                return user.Id == league.Creator;
            }
        }

        [HttpPost("")]
        public async Task<League> Create([FromBody] CreateLeagueRequest request)
        {
            var user = this.userGetter.Get(HttpContext.User.Identity as ClaimsIdentity);

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                var league = await unitOfWork.LeagueRepository.Create(
                    League.Create(
                        request.Name, 
                        request.Private, 
                        request.LeagueType, 
                        user.Id));
                var team = await unitOfWork.TeamRepository.GetById(user.Id);

                await unitOfWork.TeamRepository.Update(team.Id, team.TeamName, league.Id);

                await unitOfWork.Save();

                return league;
            }
        }

        [HttpPatch("{id:int}")]
        public async Task<IActionResult> Update([FromRoute]int id, [FromBody] UpdateLeagueRequest request)
        {
            var user = this.userGetter.Get(HttpContext.User.Identity as ClaimsIdentity);

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                var league = await unitOfWork.LeagueRepository.GetById(id);

                if (league.Creator != user.Id)
                {
                    throw new ForbiddenRequestException();
                }

                if (league.PlayersPicked)
                {
                    throw new InvalidRequestException();
                }

                league = league with { Name = request.Name, LeagueType = request.LeagueType, IsPrivate = request.Private };

                await unitOfWork.LeagueRepository.Update(league);      
                await unitOfWork.Save();

                return Ok();
            }
        }

        [HttpPost("join")]
        public async Task<JoinLeagueDTO> Join([FromBody]JoinLeagueRequest request)
        {
            var user = this.userGetter.Get(HttpContext.User.Identity as ClaimsIdentity);

            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                var league = await unitOfWork.LeagueRepository.GetByJoinCode(request.JoinCode);
                var team = await unitOfWork.TeamRepository.GetById(user.Id);

                await unitOfWork.TeamRepository.Update(user.Id, team.TeamName, league.Id);
                await unitOfWork.Save();
                return new JoinLeagueDTO(league.Id);
            }
        }
    }
}
