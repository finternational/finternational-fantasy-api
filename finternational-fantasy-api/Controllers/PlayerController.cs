﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.DTOs;
using finternational_fantasy_api.Security;
using Microsoft.AspNetCore.Mvc;

namespace finternational_fantasy_api.Controllers
{
    [ApiController]
    [Route("players")]
    public class PlayerController : ControllerBase
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;

        public PlayerController(IUnitOfWorkProvider unitOfWorkProvider, PersistenceSettings persistenceSettings)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
        }

        [HttpGet("")]
        public async Task<List<Player>> Get([FromQuery] int pageSize = 2500, [FromQuery] int pageNumber = 1, [FromQuery] int? Country = null, [FromQuery] int? Position = null)
        {
            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                return await unitOfWork.PlayerRepository.Get(pageSize, pageNumber, Country, Position);
            }
        }

        [HttpGet("{playerId:int}/events")]
        public async Task<List<Event>> GetEventsByPlayerId([FromRoute] int playerId)
        {
            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                return await unitOfWork.EventRepository.GetByPlayerId(playerId);
            }
        }
    }
}
