﻿using AutoMapper;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using finternational_fantasy_api.DTOs;
using finternational_fantasy_api.Mapping;
using Microsoft.AspNetCore.Mvc;

namespace finternational_fantasy_api.Controllers
{
    [ApiController]
    [Route("players")]
    public class PerformanceController : ControllerBase
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;
        private readonly IMapper mapper;

        public PerformanceController(
            IUnitOfWorkProvider unitOfWorkProvider, 
            PersistenceSettings persistenceSettings,
            IMapper mapper)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
            this.mapper = mapper;
        }

        [HttpGet("{playerId:int}/performances")]
        public async Task<List<PerformanceDTO>> GetByPlayerId([FromRoute] int playerId)
        {
            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                var performances = await unitOfWork.PerformanceRepository.GetByPlayerId(playerId);

                return this.mapper.Map<List<ExtendedPerformance>, List<PerformanceDTO>>(performances.OrderBy(x => x.Gameweek).ToList());
            }
        }
    }
}
