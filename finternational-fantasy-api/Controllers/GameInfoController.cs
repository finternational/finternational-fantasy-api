﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Configuration;
using Microsoft.AspNetCore.Mvc;

namespace finternational_fantasy_api.Controllers
{
    [ApiController]
    [Route("gameinfo")]
    public class GameInfoController : ControllerBase
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;

        public GameInfoController(IUnitOfWorkProvider unitOfWorkProvider, PersistenceSettings persistenceSettings)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
        }

        [HttpGet("game/{gameId:int}")]
        public async Task<GameInfo> GetByGameId([FromRoute] int gameId)
        {
            using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
            {
                return await unitOfWork.GameInfoRepository.GetByGameId(gameId);
            }
        }
    }
}
