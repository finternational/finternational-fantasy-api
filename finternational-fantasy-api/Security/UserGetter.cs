﻿using finternational_fantasy_api.Exceptions;
using System.Security;
using System.Security.Claims;

namespace finternational_fantasy_api.Security
{
    public class UserGetter : IUserGetter
    {
        public User Get(ClaimsIdentity identity)
        {
            try
            {
                var claim = identity.Claims;
                #pragma warning disable CS8602 // Dereference of a possibly null reference.
                return new User(
                        Guid.Parse(claim.Where(x => x.Type == ClaimTypes.NameIdentifier).FirstOrDefault().Value),
                        claim.Where(x => x.Type == ClaimTypes.GivenName).FirstOrDefault().Value,
                        claim.Where(x => x.Type == ClaimTypes.Surname).FirstOrDefault().Value);
                #pragma warning restore CS8602 // Dereference of a possibly null reference.
            }
            catch (NullReferenceException)
            {
                throw new SecurityException();
            }
        }
    }
}
