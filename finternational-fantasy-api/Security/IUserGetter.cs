﻿using System.Security.Claims;

namespace finternational_fantasy_api.Security
{
    public interface IUserGetter
    {
        User Get(ClaimsIdentity identity);
    }
}
