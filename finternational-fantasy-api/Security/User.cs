﻿namespace finternational_fantasy_api.Security
{
    public record User(Guid Id, string FirstName, string SecondName) {}
}
