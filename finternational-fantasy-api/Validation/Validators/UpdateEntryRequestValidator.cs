﻿using finternational_common.Entities;
using finternational_fantasy_api.Requests;
using FluentValidation;
using static finternational_fantasy_api.Requests.UpdateEntryRequest;

namespace finternational_fantasy_api.Validation.Validators
{ 
    public class UpdateEntryRequestValidator : AbstractValidator<UpdateEntryRequest>
    {
        public UpdateEntryRequestValidator()
        {
            RuleFor(x => x.Players.Count).Equal(15).WithMessage("Request must contain 15 players");
            RuleFor(x => x.Players.Where(x => !x.Substitute).Count()).Equal(11).WithMessage("Request must contain 11 first team players");
            //RuleFor(x => x.Players).Must(ValidCaptain).WithMessage("Request does not have a valid captain");
            //RuleFor(x => x.Players).Must(ValidViceCaptain).WithMessage("Request does not have a valid vice captain");
            RuleFor(x => x.Players).Must(UniquePlayers).WithMessage("Request containers 2 or more of the same player");
            RuleFor(x => x.Players).Must(UniqueSquadPositions).WithMessage("Request contains 2 or more of the same squad position");
        }

        private bool ValidCaptain(List<EntryPlayer> players)
        {
            return players.Where(x => x.Captain).Count() == 1
                && !players.Where(x => x.Captain).First().Substitute 
                && players.Where(x => x.Captain).First().PlayerId != players.Where(x => x.ViceCaptain).FirstOrDefault()?.PlayerId;
        }

        private bool ValidViceCaptain(List<EntryPlayer> players)
        {
            return players.Where(x => x.ViceCaptain).Count() == 1 && !players.Where(x => x.ViceCaptain).First().Substitute;
        }

        private bool UniquePlayers(List<EntryPlayer> players)
        {
            return players.GroupBy(x => x.PlayerId).Count() == players.Count();
        }

        private bool UniqueSquadPositions(List<EntryPlayer> players)
        {
            return players.GroupBy(x => x.SquadPosition).Count() == players.Count();
        }
    }
}
