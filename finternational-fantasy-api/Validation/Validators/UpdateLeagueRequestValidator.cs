﻿using finternational_common.Enums;
using finternational_fantasy_api.DTOs.Request;
using FluentValidation;

namespace finternational_fantasy_api.Validation.Validators
{
    public class UpdateLeagueRequestValidator : AbstractValidator<UpdateLeagueRequest>
    {
        public UpdateLeagueRequestValidator()
        {
            RuleFor(x => x.Name).NotEmpty().Length(1, 50);
            RuleFor(x => x.LeagueType).Must(x => Enum.IsDefined(typeof(LeagueType), x));
        }
    }
}
