﻿using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Requests;
using finternational_fantasy_api.Security;

namespace finternational_fantasy_api.Verification
{
    public interface IUpdateEntryRequestVerifier
    {
        Task Verify(IUnitOfWork unitOfWork, UpdateEntryRequest request, User user, int entryId);
    }
}
