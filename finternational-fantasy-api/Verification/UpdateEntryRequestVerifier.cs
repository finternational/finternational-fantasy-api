﻿using finternational_common.Enums;
using finternational_common.Exceptions;
using finternational_common.Persistence.Interfaces;
using finternational_fantasy_api.Exceptions;
using finternational_fantasy_api.Requests;
using finternational_fantasy_api.Security;
using System.Security;

namespace finternational_fantasy_api.Verification
{
    public class UpdateEntryRequestVerifier : IUpdateEntryRequestVerifier
    {
        public async Task Verify(IUnitOfWork unitOfWork, UpdateEntryRequest request, User user, int entryId)
        {
            try
            {
                var gameweektask = unitOfWork.GameweekRepository.GetByEntryId(entryId);
                var playersTask = unitOfWork.PlayerRepository.FindByIds(request.Players.Select(p => p.PlayerId).ToList());
                var entryTask = unitOfWork.EntryRepository(user.Id).GetById(entryId);

                await Task.WhenAll(gameweektask, playersTask, entryTask);

                var gameweek = await gameweektask;
                var players = await playersTask;
                var entry = await entryTask;

                if(entry.TeamId != user.Id)
                {
                    throw new SecurityException();
                }

                //should think about different timezones if we ever want to expand this
                if (DateTime.Now > gameweek.Deadline)
                {
                    throw new DeadlinePassedException();
                }

                if (!gameweek.Next)
                {
                    throw new BadHttpRequestException("", 400);
                }

                var verifiedPlayers = (from requestPlayer in request.Players
                                       join dbPlayer in players
                                           on requestPlayer.PlayerId equals dbPlayer.Id
                                       select new VerifiedEntryRequestPlayer(
                                           dbPlayer.Id,
                                           dbPlayer.Position,
                                           requestPlayer.Substitute,
                                           dbPlayer.Country.Id)).ToList();

                VerifySquad(verifiedPlayers);
                VerifyTeam(verifiedPlayers);
            } 
            catch (EntityNotFoundException)
            {
                throw new BadHttpRequestException("", 400);
            }
        }

        private void VerifySquad(List<VerifiedEntryRequestPlayer> players)
        {
            if (players.Count != 15)
            {
                throw new InvalidRequestException("One or more players is not valid");
            }
            if(players.Where(x => x.Position == (int)PositionEnum.Goalkeeper).Count() != 2)
            {
                throw new InvalidRequestException("Incorrect number of goalkeepers in squad");
            }
            if(players.Where(x => x.Position == (int)PositionEnum.Defender).Count() != 5)
            {
                throw new InvalidRequestException("Incorrect number of defenders in squad");
            }
            if(players.Where(x => x.Position == (int)PositionEnum.Midfielder).Count() != 5)
            {
                throw new InvalidRequestException("Incorrect number of midfielders in squad");
            }
            if(players.Where(x => x.Position == (int)PositionEnum.Attacker).Count() != 3)
            {
                throw new InvalidRequestException("Incorrect number of attackers in squad");
            }
            if(players.GroupBy(x => x.Country).Any(country => country.Count() > 3))
            {
                throw new InvalidRequestException("Too many players from one country");
            }           
        }

        private void VerifyTeam(List<VerifiedEntryRequestPlayer> players)
        {
            if (players.Where(x => x.Position == (int)PositionEnum.Goalkeeper && !x.Substitute).Count() != 1)
            {
                throw new InvalidRequestException("Incorrect number of goalkeepers in team");
            }
            if(players.Where(x => x.Position == (int)PositionEnum.Defender && !x.Substitute).Count() > 5 || players.Where(x => x.Position == (int)PositionEnum.Defender && !x.Substitute).Count() < 3)
            {
                throw new InvalidRequestException("Incorrect number of defenders in team");
            }
            if (players.Where(x => x.Position == (int)PositionEnum.Midfielder && !x.Substitute).Count() > 5 || players.Where(x => x.Position == (int)PositionEnum.Midfielder && !x.Substitute).Count() < 3)
            {
                throw new InvalidRequestException("Incorrect number of midfielders in team");
            }
            if (players.Where(x => x.Position == (int)PositionEnum.Attacker && !x.Substitute).Count() > 3 || players.Where(x => x.Position == (int)PositionEnum.Attacker && !x.Substitute).Count() < 1)
            {
                throw new InvalidRequestException("Incorrect number of attackers in team");
            }
        }

        private record VerifiedEntryRequestPlayer(int Id, int Position, bool Substitute, int Country) { }
    }
}
