using finternational_fantasy_api.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Microsoft.Identity.Web;
using finternational_fantasy_api.Controllers.Filters;
using FluentValidation.AspNetCore;
using finternational_fantasy_api.Messaging;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.RegisterPersistenceSettings(builder.Configuration);
builder.Services.RegisterQueueSettings(builder.Configuration);
builder.Services.AddPersistenceLayer();
builder.Services.AddMessagingLayer();
builder.Services.AddSecurityLayer();
builder.Services.AddMapping();
builder.Services.AddValidators();
builder.Services.AddVerification();
builder.Services.AddHostedService<MessagingService>();

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(b =>
    {
        b.WithOrigins(builder.Configuration.GetSection("CORS:AllowedOrigins").Get<string[]>()).AllowAnyHeader().AllowAnyMethod().WithExposedHeaders("*");
    });
});

builder.Services.AddAuthentication("Bearer")
        .AddMicrosoftIdentityWebApi(builder.Configuration.GetSection("AzureAdB2C"));

builder.Services.AddAuthorization(options =>
{
    options.FallbackPolicy = new AuthorizationPolicyBuilder("Bearer")
       .RequireAuthenticatedUser()
       .Build();
});


builder.Services.AddControllers(options =>
{
    options.Filters.Add(new ExceptionFilter());

}).AddFluentValidation();


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.OAuth2,
        Flows = new OpenApiOAuthFlows
        {
            Implicit = new OpenApiOAuthFlow
            {
                AuthorizationUrl = new Uri(builder.Configuration.GetValue<string>("Swagger:B2C:AuthorizationUrl")),
                Scopes = new Dictionary<string, string>
                {
                    { builder.Configuration.GetValue<string>("Swagger:B2C:Scope"), "User access" },
                }
            }
        },
    });
    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
            },
            new string[] { builder.Configuration.GetValue<string>("Swagger:B2C:Scope") }
        }
    });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (builder.Configuration.GetValue<bool>("Swagger:Enabled"))
{
    app.UseSwagger();
    app.UseSwaggerUI(
        options =>
        {
            options.OAuthClientId(builder.Configuration.GetValue<string>("Swagger:B2C:ClientId"));                    
        });
}


app.UseHttpsRedirection();

app.UseCors();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();

app.Run();
